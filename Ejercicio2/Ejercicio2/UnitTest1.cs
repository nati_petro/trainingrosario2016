﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using p1e1;

namespace Ejercicio2
{
    [TestClass]
    public class CalcularMayorMenorPromedio
    {
        [TestMethod]
        public void esMayor()
        {
            var listaTestigo = new List<int>();
            listaTestigo.Add(4);
            listaTestigo.Add(8);
            listaTestigo.Add(12);
            Assert.AreEqual(12, e1sol.CalcularMayor(listaTestigo));
        }

        [TestMethod]
        public void esMenor()
        {
            var listaTestigo = new List<int>();
            listaTestigo.Add(4);
            listaTestigo.Add(8);
            listaTestigo.Add(12);
            Assert.AreEqual(4, e1sol.CalcularMenor(listaTestigo));
        }

        [TestMethod]
        public void esMayorWithNeg()
        {
            var listaTestigo = new List<int>();
            listaTestigo.Add(-4);
            listaTestigo.Add(-8);
            listaTestigo.Add(-12);
            Assert.AreEqual(-4, e1sol.CalcularMayor(listaTestigo));
        }

        [TestMethod]
        public void promedio()
        {
            var listaTestigo = new List<int>();
            listaTestigo.Add(4);
            listaTestigo.Add(8);
            listaTestigo.Add(12);
            Assert.AreEqual(8, e1sol.CalcularProm(listaTestigo));
        }

        [TestMethod]
        public void promedioWithNegs()
        {
            var listaTestigo = new List<int>();
            listaTestigo.Add(-4);
            listaTestigo.Add(-8);
            listaTestigo.Add(-12);
            Assert.AreEqual(-8, e1sol.CalcularProm(listaTestigo));
        }

       
    }
}
