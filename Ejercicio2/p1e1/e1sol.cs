﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p1e1
{
    public class e1sol
    {

        public static int CalcularMayor(List<int> list)
        {
            int num = list[0];
            foreach (var item in list)
            {
                if (num < item)
                {
                    num = item;
                }
            }
            return num;
        }

        public static int CalcularMenor(List<int> list)
        {
            int num = list[0];
            foreach (var item in list)
            {
                if (num > item)
                {
                    num = item;
                }
            }
            return num;
        }

        public static float CalcularProm(List<int> list)
        {

            float prom = 0;
            int sum = 0;
            foreach (var item in list)
            {
                sum = sum + item;

            }
            prom = sum / list.Count;
            return prom;
        }
    }
}
