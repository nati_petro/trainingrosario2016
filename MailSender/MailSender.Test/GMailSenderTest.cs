﻿namespace MailSender.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    [TestClass]
    public class GMailSenderTest
    {
        private IMailSender _mailSender;

        [TestInitialize]
        public void Setup()
        {
            this._mailSender = new GMailSender();
        }

        [TestMethod]
        public void SendMail()
        {
            var message = new Mail
            {
                To = "sistemareservas.noresponder@gmail.com",
                Body = "Integration Test, GMailSender.SendMail() " + DateTime.Now,
                Subject = "Integration Test"
            };
            this._mailSender.SendMail(message);
            Assert.IsTrue(true);
        }
    }
}
